//
//  PushOverHandler.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let postPushMessageURL = "https://api.pushover.net/1/messages.json"

func postPushMessage(_ message: MessageRealm, appToken: String) {
    let parameters: Parameters = ["token": appToken, "user": message.reciever, "title": message.title, "message": message.text]
    
    Alamofire.request(postPushMessageURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
        .responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                if let status = json["status"].int {
                    if status == 1 {
                        addMessage(message: message)
                        
                        NotificationCenter.default.post(name: Notification.Name("postPushMessageComplietedWithSuccess"), object: nil)
                    } else if status == 0 {
                        var errorsString = ""
                        if let error = json["errors"].array {
                            error.forEach({ (err) in
                                if let errorString = err.string {
                                    errorsString.append(errorString)
                                    errorsString.append("\n")
                                }
                            })
                        }
                        
                        NotificationCenter.default.post(name:Notification.Name("postPushMessageComplietedWithError"), object: errorsString)
                    } else {
                        NotificationCenter.default.post(name:Notification.Name("postPushMessageComplietedWithCriticalError"), object: nil)
                    }
                }
            case .failure(let error):
                print(error)
                
                NotificationCenter.default.post(name:Notification.Name("postPushMessageComplietedWithCriticalError"), object: nil)
            }
    }
}
