//
//  MessagesHandler.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

var messagesRealm: [MessageRealm] = [] {
    didSet {
        NotificationCenter.default.post(name: Notification.Name("reloadMessagesTable"), object: nil)
    }
}

func loadMessages() {
    if let realm = try? Realm() {
        messagesRealm = Array(realm.objects(MessageRealm.self).sorted(byKeyPath: "id").reversed())
    }
}

func addMessage(message: MessageRealm) {
    if let realm = try? Realm() {
        try? realm.write {
            realm.add(message)
        }
    }
}

func removeMessage(messageID: Int) {
    if let realm = try? Realm() {
        try? realm.write {
            if let message = realm.objects(MessageRealm.self).filter("id = " + String(messageID)).first {
                realm.delete(message)
            }
        }
    }
}
