//
//  MessageDetailsViewController.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import UIKit

class MessageDetailsViewController: UIViewController {

    @IBOutlet weak var messageDetailsRecieverLabel: UILabel!
    @IBOutlet weak var messageDetailsTitleLabel: UILabel!
    @IBOutlet weak var messageDetailsTextLabel: UILabel!
    @IBOutlet weak var messageDetailsDateLabel: UILabel!
    
    var messageRealm: MessageRealm = MessageRealm()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        messageDetailsRecieverLabel.text = messageRealm.reciever
        messageDetailsTitleLabel.text = messageRealm.title
        messageDetailsTextLabel.text = messageRealm.text
        
        dateFormatter.dateFormat = "HH:mm:ss, dd.MM.yyyy"
        messageDetailsDateLabel.text = dateFormatter.string(from: messageRealm.date)
    }

}
