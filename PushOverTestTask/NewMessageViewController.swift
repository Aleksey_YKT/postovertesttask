//
//  NewMessageViewController.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import UIKit

class NewMessageViewController: UIViewController {

    @IBOutlet weak var newMessageScrollView: UIScrollView!
    @IBOutlet weak var newMessageAppTokenTextField: UITextField!
    @IBOutlet weak var newMessageRecieverTextField: UITextField!
    @IBOutlet weak var newMessageTitleTextField: UITextField!
    @IBOutlet weak var newMessageTextTextView: UITextView!
    @IBOutlet weak var newMessageSendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTextFieldStyles()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        newMessageSendButton.addTarget(self, action: #selector(newMessageSendButtonPressed(_:)), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(postPushMessageComplietedWithSuccess(_:)), name: Notification.Name("postPushMessageComplietedWithSuccess"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(postPushMessageComplietedWithError(_:)), name: Notification.Name("postPushMessageComplietedWithError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(postPushMessageComplietedWithCriticalError(_:)), name: Notification.Name("postPushMessageComplietedWithCriticalError"), object: nil)
        
        newMessageAppTokenTextField.text = "azng4zwq8ibsobufo77v6532is19d8"
        newMessageRecieverTextField.text = "u3ba37zs37ftt72cab43qwp2xofidn"
    }
    
    @objc func newMessageSendButtonPressed(_ button: UIButton) {
        self.view.endEditing(true)
        
        if let appToken = newMessageAppTokenTextField.text, !appToken.isEmpty {
            if let reciever = newMessageRecieverTextField.text, !reciever.isEmpty {
                if let title = newMessageTitleTextField.text, !title.isEmpty {
                    if let text = newMessageTextTextView.text, !text.isEmpty {
                        let messageRealm = MessageRealm()
                        messageRealm.id = messageRealm.IncrementaID()
                        messageRealm.reciever = reciever
                        messageRealm.title = title
                        messageRealm.text = text
                        messageRealm.date = Date()
                        
                        postPushMessage(messageRealm, appToken: appToken)
                    }
                }
            }
        }
    }
    
    @objc func postPushMessageComplietedWithSuccess(_ notification: NSNotification) {
        loadMessages()
        
        let alert = UIAlertController(title: "Готово", message: "Сообщение было успешно отправлено", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ок", style: .default, handler: { (alert: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(alertAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func postPushMessageComplietedWithError(_ notification: NSNotification) {
        if let error = notification.object as? String {
            alertView("Ошибка", message: error, titleAction: "Ок")
        }
    }
    
    @objc func postPushMessageComplietedWithCriticalError(_ notification: NSNotification) {
        alertView("Критическая ошибка", message: "В момент отправки сообщения произошла критическая ошибка", titleAction: "Ок")
    }
    
    func setupTextFieldStyles() {
        newMessageAppTokenTextField.layer.masksToBounds = false
        newMessageAppTokenTextField.layer.cornerRadius = 4.0
        newMessageAppTokenTextField.layer.borderWidth = 0.3
        newMessageAppTokenTextField.layer.borderColor = UIColor.gray.cgColor
        
        newMessageRecieverTextField.layer.masksToBounds = false
        newMessageRecieverTextField.layer.cornerRadius = 4.0
        newMessageRecieverTextField.layer.borderWidth = 0.3
        newMessageRecieverTextField.layer.borderColor = UIColor.gray.cgColor
        
        newMessageTitleTextField.layer.masksToBounds = false
        newMessageTitleTextField.layer.cornerRadius = 4.0
        newMessageTitleTextField.layer.borderWidth = 0.3
        newMessageTitleTextField.layer.borderColor = UIColor.gray.cgColor
        
        newMessageTextTextView.layer.masksToBounds = false
        newMessageTextTextView.layer.cornerRadius = 4.0
        newMessageTextTextView.layer.borderWidth = 0.3
        newMessageTextTextView.layer.borderColor = UIColor.gray.cgColor
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if let info:NSDictionary = notification.userInfo as NSDictionary? {
            if let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? Double {
                if let curve = info[UIKeyboardAnimationCurveUserInfoKey] as? UInt {
                    self.view.setNeedsLayout()
                    
                    self.newMessageScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    
                    self.view.setNeedsUpdateConstraints()
                    
                    UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: {
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
    
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        if let info:NSDictionary = notification.userInfo as NSDictionary? {
            if let keyboardHeight = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
                if let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? Double {
                    if let curve = info[UIKeyboardAnimationCurveUserInfoKey] as? UInt {
                        self.view.setNeedsLayout()
                        
                        self.newMessageScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
                        
                        self.view.setNeedsUpdateConstraints()
                        
                        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: {
                            self.view.layoutIfNeeded()
                        }, completion: nil)
                    }
                }
            }
        }
    }
    
    func alertView(_ title: String, message: String, titleAction: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: titleAction, style: .default, handler: nil)
        alert.addAction(alertAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
