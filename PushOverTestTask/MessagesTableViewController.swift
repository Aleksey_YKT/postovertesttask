//
//  MessagesTableViewController.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import UIKit

class MessagesTableViewController: UITableViewController {

    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMessages()
        
        self.navigationController?.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData(_:)), name: Notification.Name("reloadMessagesTable"), object: nil)
    }

    // MARK: - Table view data source
    @objc func reloadTableData(_ notification: NSNotification) {
        if !messagesRealm.isEmpty {
            tableView.reloadData()
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return messagesRealm.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messagesTableViewCell", for: indexPath) as! MessagesTableViewCell
    
        // Configure the cell...
        cell.messageTitleLabel.text = messagesRealm[indexPath.row].title
        cell.messageTextLabel.text = messagesRealm[indexPath.row].text
        
        dateFormatter.dateFormat = "dd.MM.yyyy"
        cell.messageDateLabel.text = dateFormatter.string(from: messagesRealm[indexPath.row].date)
        
        dateFormatter.dateFormat = "HH:mm:ss"
        cell.messageTimeLabel.text = dateFormatter.string(from: messagesRealm[indexPath.row].date)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showMessageDetailsViewController", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            removeMessage(messageID: messagesRealm[indexPath.row].id)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MessageDetailsViewController {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                destination.messageRealm = messagesRealm[indexPath.row]
                
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }
    }

}

extension MessagesTableViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if navigationController.viewControllers.count > 1 {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        } else {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
}
