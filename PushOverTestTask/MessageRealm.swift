//
//  MessageRealm.swift
//  PushOverTestTask
//
//  Created by Aleksey Ivanov on 15.09.2018.
//  Copyright © 2018 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class MessageRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var reciever: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var date: Date = Date()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int {
        let realm = try! Realm()
        return (realm.objects(MessageRealm.self).sorted(byKeyPath: "id").last?.id ?? 0) + 1
    }
}
